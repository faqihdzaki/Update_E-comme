@extends('layouts.app')

@section ('content')

<div class="container p-0 show">
   <div class="row sixtyvh">
       <div class="col-lg-7 col-sm-10 mb-3 show-picture" >
            <img src="{{ asset('/storage/'.$product->image) }}" alt="">
       </div>
       <div class="col-lg-4 col-sm-12 pl-5 pr-5">
        <h6><strong>{{ $product->brand }}</strong></h6>
        <h5>{{ $product->name }}</h5>
            <div class="card">
                <div class="card-body">
                    <div class="show-info">
                        <div class="info-1">
                            <h6>BUY NOW</h6>
                        </div>
                        <div class="info-2">
                        
                        </div>
                            <h7>Price</h7>
                                <h8 >Rp {{ number_format($product->price) }} </h8>
                                <br>

                                @if($product->quantity >= 1)
                                      <span class="badge bg-success" style="color:white"> <i class="fa fa-check"></i> Ready Stock</span>
                                      @else
                                      <span class="badge bg-light text-dark"> <i class="fa fa-times"></i> Sold Out</span>
                                      @endif

                        <div class="info-3">
                            <p>{{ $product->description }}</p>
                        </div>

                        <div class="info-5">
                            <h9 style="text-align:center;"><strong>Quantity</strong></h9>
                            
                            <!-- <input type="hidden" class="product_id" value="{{ $product['quantity'] }}" >
                            <div class="input-group quantity">
                               <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                             <span class="input-group-text">-</span>
                                </div>
                                <input type="text" class="qty-input form-control" maxlength="2" max="10" value="1">
                                <div class="input-group-append increment-btn" style="cursor: pointer">
                                    <span class="input-group-text">+</span>
                                </div>
                            </div> -->
                
                            <br>
                        </div >
                        <form action="{{ route('cart.add') }}" method="post">
                            @csrf
                            <input type="hidden" name="product" value="{{ $product->id }}">
                            <input id= "quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{old ('quantity') }}" required autocomplete="name" autofocus>
                                @error('quantity')
                                <span class="invalid-feedback" role="alert">
                                <storng>{{message}}</strong>
                                </span>
                                @enderror 
                                <br>
                            <button type="submit" id="add-to-cart" class="add-to-cart enabled">
                                <div class="info-4" style = "text-align: center">
                                    ADD TO CART
                                </div>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>

@endsection