@extends('layouts.app')

@section ('content')

<div class="container p-0">
  <div class="row">

    <div class="col-lg-3 col-md-3 col-sm-4 col-5 pl-4 filter">
      <div class="fixedfilter">

        <h3><i class="fa fa-filter"></i> Filter </h3>
        <input class="mt-3" type="text" id="search" placeholder="Enter Product name" style="width:100%;">

        <div class="filterprice card">
          <div class="card-body">
            <h5 class="card-title"><strong>Price</strong></h5>
            <input type="range" min="{{ $minPrice }}" max="{{ $maxPrice }}" value="{{ $maxPrice }}" class="slider selector" id="pricerange">
            <p class="p-0 m-0">Max: Rp <span id="currentrange">{{ $maxPrice }}</span></p>
          </div>
        </div>
        
        
        <div class="filtertype card">
          <div class="card-body">
            <h5 class="card-title"><strong>Type</strong></h5>
            @foreach($types as $types)
            <input type="checkbox" id="{{ $types['type'] }}" class="type selector" name="type" value="{{ $types['type'] }}" >
            <label for="{{ $types['type'] }}">{{ \DB::table('type')->where('id_type', $types->type)->first()->type??'' }}</label><br>
            <!-- <a href="product/{{$types->type}}">{{ \DB::table('type')->where('id_type', $types->type)->first()->type??'' }}<br></a> -->
            @endforeach
          </div>
        </div>
        <div class="filtercategory card">
          <div class="card-body">
            <h5 class="card-title"><strong>Category</strong></h5>
            @foreach($categorys as $categorys)
            <input type="checkbox" id="{{ $categorys['category'] }}" class="category selector" name="category" value="{{ $categorys['category'] }}" >
            <label for="{{ $categorys['category'] }}">{{ \DB::table('category')->where('id_category', $categorys->category)->first()->category??'' }}</label><br>
            <!-- <a href="product/{{$categorys->category}}">{{ \DB::table('category')->where('id_category', $categorys->category)->first()->category??'' }}<br></a> -->
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-8 col-7 pr-4">
      <h3>Products</h3>
      
      <div class="row d-flex justify-content-start" id="products"> 

        <!-- @foreach($products as $product)
        <div class="col-lg-4 col-md-6 col-sm-12 pt-3">
          <div class="card">
            <a href="product/{{$product->id}}">
              <div class="card-body ">
                <div class="product-info">
                 <div class="info-1"><img src="{{asset('/storage/'.$product->image)}}" alt=""></div>
                 <div class="info-4"><h5>{{ \DB::table('category')->where('id_category', $product->category)->first()->category??'' }}</h5></div>
                 <div class="info-2"><h4>{{$product->name}}</h4></div>
                 <div class="info-3"><h5>Rp {{number_format ($product->price)}}</h5></div>
               </div>
             </div>
           </a>
         </div>
       </div>
       @endforeach -->
     </div> 
     <div class="pull-right">
      <hr><?php echo $products->links(); ?></hr>
    </div>
  </div>
</div>
</div>



@endsection