
@yield('script')
<script>

    $(document).ready(function(){

        filter_data('');

        function filter_data(query='')
        {
            var search=JSON.stringify(query);
            var price =JSON.stringify($('#pricerange').val());
            var category =JSON.stringify(get_filter('category')); 
            var type =JSON.stringify(get_filter('type'));
            $.ajax({
                url:"{{ route('product.filter') }}",
                method:'GET',
                data:{
                    query:search,
                    price:price,
                    type:type,
                    category:category,
                    },
                dataType:'json',
                success:function(data)
                {
                    $('#products').html(data.table_data);
                }
                
            })
        }

            //         $(document).ready(function () {

            // $('.increment-btn').click(function (e) {
            //     e.preventDefault();
            //     var incre_value = $(this).parents('.quantity').find('.qty-input').val();
            //     var value = parseInt(incre_value, 10);
            //     value = isNaN(value) ? 0 : value;
            //     if(value<10){
            //         value++;
            //         $(this).parents('.quantity').find('.qty-input').val(value);
            //     }

            // });

            // $('.decrement-btn').click(function (e) {
            //     e.preventDefault();
            //     var decre_value = $(this).parents('.quantity').find('.qty-input').val();
            //     var value = parseInt(decre_value, 10);
            //     value = isNaN(value) ? 0 : value;
            //     if(value>1){
            //         value--;
            //         $(this).parents('.quantity').find('.qty-input').val(value);
            //     }
            // });

            // });

        function get_filter(class_name)
        {
            var filter=[];
            $('.'+class_name+':checked').each(function(){
                filter.push($(this).val());
            });
            return filter;
        }

        $(document).on('keyup','#search',function(){
            var query = $(this).val();
            filter_data(query);
        });

        $('.selector').click(function(){
            var query = $('#search').val();
            filter_data(query);
        });

        $(document).on('input','#pricerange',function(){
            var range = $(this).val();
            $('#currentrange').html(range);
        });

        $(document).on('change','#size-dropdown',function(){
            var size = $(this).val();
            document.cookie="shoes_size="+size+";"+"path=/";
            $('#add-to-cart').removeClass('disabled');
        });

    });
    
</script>

</html>