@extends('layouts.admin')

@section ('content')

<div class="col-12 col-md-12 col-sm-12 col-lg-10">
    <h5>ADD TYPE</h5>
    <hr>

    <form method="POST" action="{{ route('product.createtype') }}" enctype="multipart/form-data">
        @csrf
        <div class="row ">

            <div class="col-12">
                <label for="name" class="">{{ __('Name') }}</label>
                <div class="form-group">
                    <div>
                        <input id="name" type="text" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('name') }}" required autocomplete="type" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        
        <button type="submit" class="btn btn-success w-100">ADD TYPE</button>

    </form>

</div>

@endsection