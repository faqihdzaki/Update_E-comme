@extends('layouts.admin')

@section ('content')

<div class="col-12 col-md-12 col-sm-12 col-lg-10">
  @if(Session::has('success'))
  <div class="row">
    <div class="col-12">
      <div id="charge-message" class="alert alert-success">
        {{ Session::get('success') }}
      </div>
    </div>
  </div>
  @endif
  <div class="card">
    <div class="card-header">
      <h5>TYPE LIST</h5>
    </div>
    <div class="card-body">
      <a href="{{ route('admin.addtype') }}" class="btn btn-success mb-4" style="color:white; width:150px;">ADD TYPE</a>
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Type</th>                    
          </tr>
        </thead>
        <tbody>
          @foreach ($type as $type)
          <tr>
            <th scope="row">{{ $type->id_type }}</th>                    
            <td>{{ $type->type }}</td>                    
            
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><br>
  <div class="card">
    <div class="card-header">
      <h5>CATEGORY LIST</h5>
    </div>
    <div class="card-body">
      <a href="{{ route('admin.addcategory') }}" class="btn btn-success mb-4" style="color:white; width:150px;">ADD CATEGORY</a>
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Type</th>     
            <th scope="col">Category</th>     
          </tr>
        </thead>
        <tbody>
          @foreach ($category as $category)
         <tr>
          <th scope="row">{{ $category->id_category }}</th>     
          <td scope="row">{{ \DB::table('type')->where('id_type', $category->id_type)->first()->type??'' }}</td>                    
          <td>{{ $category->category }}</td>                    
          <td>

          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection