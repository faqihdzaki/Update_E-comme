@extends('layouts.admin')

@section ('content')

<div class="col-12 col-md-12 col-sm-12 col-lg-10">
    <h5>ADD CATEGORY</h5>
    <hr>

    <form method="POST" action="{{ route('product.createcategory') }}" enctype="multipart/form-data">
        @csrf
        <div class="row ">
            <div class="col-12">
                <label for="name" class="">{{ __('Type') }}</label>
                <div class="form-group">
                    <div>
                        <select name="id_type" id="addproducttype" class="types form-control">
                            <option selected="true" value="" disabled hidden>Choose product type</option>
                            @foreach($type as $p)            
                            <option value="{{$p->id_type}}">{{$p->type}}</option>        
                            @endforeach
                            <!-- <option value="Book">Book</option>
                            <option value="Shoes">Shoes</option>
                            <option value="Stasionary">Stasionary</option>    -->                        
                        </select>
                    </div>
                </div>

            </div>
            <div class="col-12">
                <label for="name" class="">{{ __('Name') }}</label>
                <div class="form-group">
                    <div>
                        <input id="name" type="text" class="form-control @error('type') is-invalid @enderror" name="category" value="{{ old('name') }}" required autocomplete="type" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            
        </div>

        <button type="submit" class="btn btn-success w-100">ADD TYPE</button>

    </form>

</div>

@endsection