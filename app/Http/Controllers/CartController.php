<?php

namespace App\Http\Controllers;
use App;
use App\Product;
use App\Cart;
use Session;
use App\Carts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function add(Request $request)
    {
         // cek autentifikasi
         if (Auth::user() != null){
            // jika user sudah login

            // cek apakah memiliki
            $cek = Carts::where('user_id',Auth::user()->id)->first();

            if ($cek != null){
                // jika user sudah memiliki chart

                //menyimpan data kolom item dari tabel cart ke dalam variabel $item
                $item = $cek -> item;

                // perulangan untuk mengecek apakah barang yang di masukkan user sudah ada dalam chart
                foreach ($item as $i){
                    if($i->product_id==$request->product){
                        // jika sudah ada, maka mengubah jumlah quantity
                        (int)$i->quantity+=$request->quantity;
                    }else{
                        // membuat array
                        $j['product_id'] = $request->product;
                        $j['quantity'] = $request->quantity;

                        // push array ke variabel $item
                        array_push($item,$j);

                        // menghentikan perulangan
                        break;
                    }
                }
                // encode array
                $item = json_encode($item);

                // update kolom item pada tabel chart
                Carts::where('user_id',Auth::user()->id)->update([
                    'item' => $item
                ]);
            }else{
                // membuat array dengan nama $item
                $item = [];

                // membuat array dengan index dan value
                $i['product_id'] = $request->product;
                $i['quantity'] = $request->quantity;

                // push array $i ke array $item
                array_push($item,$i);

                // proses menambahkan data baru ke tabel chart
                $cart = new Carts();
                $cart -> user_id = Auth::user()->id;
                $cart -> item = json_encode($item);
                $cart->save();
            }
            return redirect()->route('cart.index');
        }else{
            // jika user belum login
            return redirect ()->route("login");
        } 
    }
 // $carts-> user_id = 
        // $product = Product::find($id);
        // $oldCart = Session::has('cart') ? Session::get('cart') : null;
        // $cart = new Cart($oldCart);
        // $cart->add ($product, $product->id);

        // return Session::get('cart');
        // $request->session()->put('cart', $cart);
        // return redirect() ->route('cart.index');
    
    public function index()
    {
        if(!Session::has('cart')){
            return view('cart.index');
        }
        // ['products'=>null]
        $oldCart= Session::get('cart');
        $products= Product::all();
        // $products = $cart->items;
        $totalPrice = 0;
        $totalQuantity= 0;
        $carts = Carts::where('user_id',Auth::user()->id)->get();
        return view('cart.index', compact ('products','totalPrice','totalQuantity','carts'));
    }
    // public function add(Request $request, $id)
    // {
    //     if(isset($_COOKIE["shoes_size"])){
    //         // $size=$_COOKIE["shoes_size"];
    //         $id=$_COOKIE["shoes_size"];
    //     }
        
    //     $product = Product::find($id);
    //     $oldCart = Session::has('cart') ? Session::get('cart') : null;
    //     $cart = new Cart($oldCart);
    //     // $cart->add($product,$product->id,$size);
    //     $cart->add($product,$product->id,$id);
    //     //dd($cart);
    //     $request->session()->put('cart',$cart);
    //     return redirect()->route('cart.index');
    // }

    public function remove($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($id);
    
        Session::put('cart',$cart);
        if($cart->totalQuantity<=0){
            Session::forget('cart');
        }
        return redirect()->route('cart.index');
    }

}