<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carts extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ["product_id", "quantity"];
    public function getItemAttribute($value){
        return json_decode($value);
    }
}
