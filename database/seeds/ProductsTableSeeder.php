<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'id'=>1,
                'name'=>'WINGIT',
                'brand'=>'Sara Wijayanto',
                'price'=>80000,
                'image'=>'products/wingit.jpg',
                'gender'=>'Female',
                'category'=>'Shoes',
                'quantity'=>1
            ],
            [
                'id'=>2,
                'name'=>'SHINE',
                'brand'=>'Jessica Jung',
                'price'=>116500,
                'image'=>'products/shine.jpg',
                'gender'=>'Unisex',
                'category'=>'Shoes',
                'quantity'=>12
            ],
            [
                'id'=>3,
                'name'=>'PENA BERACUN (The Moving Finger)"',
                'brand'=>'Agatha Cristie',
                'price'=>59000,
                'image'=>'products/pena.jpg',
                'gender'=>'Male',
                'category'=>'Shoes',
                'quantity'=>1
            ],
            [
                'id'=>4,
                'name'=>'MERAWAT MONSTERA',
                'brand'=>'Redaksi Agromedia',
                'price'=>68000,
                'image'=>'products/monstera.jpg',
                'gender'=>'Male',
                'category'=>'Shoes',
                'quantity'=>1
            ],
            [
                'id'=>5,
                'name'=>'SELENA',
                'brand'=>'Tere Liye',
                'price'=>68000,
                'image'=>'products/selena.jpg',
                'gender'=>'Male',
                'category'=>'Shoes',
                'quantity'=>14
            ],
            [
                'id'=>6,
                'name'=>'ASSASSINATION CLASHROOM 01',
                'brand'=>'Yusei Matsui',
                'price'=>20000,
                'image'=>'products/aso.jpg',
                'gender'=>'Unisex',
                'category'=>'Shoes',
                'quantity'=>3
            ],
            [
                'id'=>7,
                'name'=>'HOW NOT TO DIET: THE GROUNDBREAKING SCIENCE OF HEALTHY, PERM',
                'brand'=>'Michael Greger',
                'price'=>475000,
                'image'=>'products/7.jpg',
                'gender'=>'Male',
                'category'=>'Shoes',
                'quantity'=>5
            ],
            [
                'id'=>8,
                'name'=>'MENGUASAI PEMROGRAMAN"',
                'brand'=>'Ade Rahmat Iskandar',
                'price'=> 130000,
                'image'=>'products/8.jpg',
                'gender'=>'Unisex',
                'category'=>'Shoes',
                'quantity'=>3
            ],
            [
                'id'=>9,
                'name'=>'THE POWER OF AL-IKHLAS"',
                'brand'=>'Anin DP',
                'price'=> 34500,
                'image'=>'products/9.jpg',
                'gender'=>'Male',
                'category'=>'Shoes',
                'quantity'=>3
            ],
            [
                'id'=>10,
                'name'=>'LAUT BERCERITA',
                'brand'=>'Leila S. Chudori',
                'price'=> 80000,
                'image'=>'products/10.jpg',
                'gender'=>'Male',
                'category'=>'Shoes',
                'quantity'=>5
            ],
            [
                'id'=>11,
                'name'=>'ARTEMIS "',
                'brand'=>'Andy Wier',
                'price'=> 129000,
                'image'=>'products/11.jpg',
                'gender'=>'Female',
                'category'=>'Shoes',
                'quantity'=>3
            ],
            [
                'id'=>12,
                'name'=>'FAIRY TAIL eps 49"',
                'brand'=>'PumaHiro Mashima',
                'price'=> 20000,
                'image'=>'products/12.jpg',
                'gender'=>'Female',
                'category'=>'Shoes',
                'quantity'=>3
            ],
        

        ]);
    }
}